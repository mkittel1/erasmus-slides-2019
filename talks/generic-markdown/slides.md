# Erasmus 2019 
Maike Kittelmann, kittelmann@sub.uni-goettingen.de

[<small>Goettingen State and University Library</small>](https://www.sub.uni-goettingen.de/en/news/)

---

![SUB Central Library](img/zb.jpg)



[<small>Goettingen State and University Library</small>](https://www.sub.uni-goettingen.de/en/news/)

--

![SUB Central Library](img/zb-outside-daylight.jpg)

--

![SUB Central Library](img/zb-outside-night.jpg)

--

![SUB Historical Building](img/hg-outside.JPG)

--

![SUB Historical Building](img/hg.jpg)

--

![SUB Historical Building](img/hg-reading-room.jpg)

--

Where we are ...

![Where we are](img/göttingen-map.png)

--


"Gänseliesl" 

![Most famous woman in town](img/gl.jpg)

--


### Portrait (1/3)

Göttingen State and University Library, founded in 1734 by the English king, was the first realisation of the idea of a modern research library. 

With its current holdings of about 8 million media units, Göttingen State and University Library ranks among the largest libraries in Germany. 

--


### Portrait (2/3)

About 5,500 visitors frequent the Central Library (opened in 1993) every day. 

In recognition of its achievements, Göttingen State and University Library has received a number of awards.

--


### Portrait (3/3)

[UNESCO World Heritage Site](https://www.unesco.de/kultur-und-natur/weltdokumentenerbe/weltdokumentenerbe-deutschland/unesco-weltdokumentenerbe-42)

![Gutenberg Bible](img\005r1l.jpg)

--

[Gutenberg Bible Online](http://www.gutenbergdigital.de/gudi/eframes/index.htm)





---

# Information Literacy at SUB

<!-- .slide: data-background-color="lightyellow" -->


--

### Information Literacy at SUB 

- at the Library 

--

### Information Literacy at SUB 

- at the Library  [→ classes & tours](https://www.sub.uni-goettingen.de/en/learning-teaching/courses-guided-library-tours/)


--

### Information Literacy at SUB 

- at the Library

    - Reference Management 
        - [→ zotero](https://www.sub.uni-goettingen.de/lernen-lehren/kurse-fuehrungen/schulung/introduction-to-zotero/s/Schulung/show/) 
        - [→ citavi](https://www.sub.uni-goettingen.de/en/learning-teaching/courses-guided-library-tours/schulung/literaturverwaltung-mit-citavi-literatur-sammeln-organisieren-zitieren/s/Schulung/show/)
        - [→ endnote](https://www.sub.uni-goettingen.de/en/learning-teaching/courses-guided-library-tours/schulung/endnote-fuer-einsteiger/s/Schulung/show/)


--

![coffee](img/coffee-lectures.jpg)

--

### Information Literacy at SUB 

- at the Library

    - Coffee Lectures [→ ☕](https://www.sub.uni-goettingen.de/en/learning-teaching/courses-guided-library-tours/coffee-lectures/)

--

### Information Literacy at SUB 

- at the Library

    - Consultation Hour XXL [→ Academic Writing](https://www.sub.uni-goettingen.de/en/learning-teaching/courses-guided-library-tours/schulung/hausarbeiten-sprechstunde-xxl/s/Schulung/show/)





--

### Information Literacy at SUB 

- at home



--

### Information Literacy at SUB 

- at home 
    - [→ self-study](https://www.sub.uni-goettingen.de/en/learning-teaching/self-study-materials/)



--

### Information Literacy at SUB 

- at home 
    - Videos [→ youtube channel](https://www.youtube.com/playlist?list=PLgoiCMgV-zrfLp3DkVorGGU_MGXR5lsfY)


--

### Information Literacy at SUB 

- at home 
    - Ask a Librarian [→ 🗨️](https://www.sub.uni-goettingen.de/en/contact/ask-a-librarian/)




---

# How to find literature

<!-- .slide: data-background-color="lightblue" -->

--


[Basics for Beginners: Finding Literature](https://www.sub.uni-goettingen.de/fileadmin/media/texte/benutzung/Literaturrecherche/Basics_for_Beginners__Self-Study_Material_20181025.pdf)


--

## local

[OPAC (GUK)](https://www.sub.uni-goettingen.de/sub-aktuell/)

--

## trans-regional

[Union Catalogue GBV & SWB (GVK / k10plus)](https://www.sub.uni-goettingen.de/sub-aktuell/)

--

## PICA

[internal format and cataloguing format](https://www.gbv.de/bibliotheken/verbundbibliotheken/02Verbund/01Erschliessung/02Richtlinien/02KatRichtRDA/Format_GBV_Endfassung_pica.pdf)


1969: originally Dutch system

today: [OCLC Pica](https://en.wikipedia.org/wiki/OCLC_PICA)

--

## Library Unions in Germany

![Map Library Unions in Germany](img\Karte_Bibliotheksverbünde_Deutschland.png)



---

<!-- .slide: data-background-color="lightgreen" -->

# Cultural Heritage 

--

[Digitization Centre](https://gdz.sub.uni-goettingen.de/)

--

specialized databases & information services for researchers

--

## CERL

[Consortium of European Research Libraries](http://www.cerl.org)

![logo](img/cerl.jpg)

--

## Printed Books until 1830

[CERL Heritage of the Printed Book Database](http://hpb.cerl.org/GB-Uk.ISTC.ic00199200)

--

<small>[contributing institutions](https://www.cerl.org/resources/hpb/content)</small>

<small>[example record](https://gso.gbv.de/DB=1.77/CMD?ACT=SRCHA&IKT=1016&SRT=YOP&TRM=cid+GB-Uk.ISTC.ic00199200)</small>

---

<!-- .slide: data-background-color="lightgrey" -->

[These slides](https://tinyurl.com/itsew2019)

[https://tinyurl.com/isew2019](https://tinyurl.com/isew2019)  <!-- itsew2019oulu -->

--

<!-- .slide: data-background-color="lightgrey" -->

### Thank you very much for your attention!

Any questions?

<!--

https://tinyurl.com/iswe2019

What about a table? You can easily create one with the help of [this tool](https://www.tablesgenerator.com/markdown_tables).

| Head 1 | Head 2 | Head 3 |
|----------|--------|--------|
| centered | left | right |
| content | align | align |
-->